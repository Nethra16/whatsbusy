#function that takes in a directed graph and returns the list of network failure points
def identify_router(graph):
    
    #list to keep account of both inbound and outbound links initialized to 0 
    allLinks=[]

    for i in range(N):
        allLinks.append(0)
    
    
    for key in graph:
        #for every label in the route, make a count+1 for every connection

        if graph[key]!=None:
            for value in graph[key]:
                allLinks[key-1]+=1
                allLinks[value-1]+=1
    
    #list to be retured with all the max-value indicies
    maxIndices=[]

    #number of max links any label has in the given route
    maxLinks=max(allLinks)

    # print(maxLinks)
    #appending all the indicies to the list
    for i in range(len(allLinks)):
        if allLinks[i]==maxLinks:
            maxIndices.append(i+1)
    

    return maxIndices
    
    




if __name__ == "__main__":
    
    #time complexity
    #details are printed on runnin the program
    print("---------------------------------")
    print("\nTIME COMPLEXITY")
    print("\nSay there are n labels in a route and the number of outbound links every label has is denoted by its degree(deg), then the time complexity would be O(Σ(deg(n))).\nThat is the sum of all the outbound links of every label in the route.\n")
    print("The worst case be that every label is linked to every other label except itself, so the worst case complexity would be O(n*(n-1)) that is inturn O(n^2).")
    print("\nHence, O(Σ(deg(n))) is the best general time complexity we can acquire and that is what this function has. It visits every inbound and outbound label exactly once and thus the time complexity of O(Σ(deg(n))).")
    print("\n---------------------------------")
    
    
    #number of labels
    global N
    N=6

    #graph using a dictionary where all the outbound labels are set to None
    graph={i+1: None for i in range(N)}

    route=list(map(int,input("Enter the route with space separated labels(Ex: '1 2 3'): ").split()))

    #using the given route, make a directional graph where key is the outbound label and the value is the inbound label
    for i in range(len(route)-1):
        key=route[i]
        value=route[i+1]
        if not graph[key]:
            graph[key]=[value]
        else:
            graph[key].append(value)
    print(graph)

    #calling the indentify_router function that returns all the max-value indices
    labels=identify_router(graph)
    print(labels)

