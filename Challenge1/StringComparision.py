#compress function which takes a string as argument and returns the compressed string
def compression(old_str):
    new_str=""
    
    #variables to keep account for current letter, it's count and position
    count=0
    position=0
    letter=old_str[position]
    
    #this loop runs until the end of string to count each letter
    while position<len(old_str):
        #keeps incrementing count until another letter is found or string has ended
        if old_str[position]==letter:
            count=count+1
            position=position+1

            #if the next letter is different
            #then add the current letter count and proceed
            if position<len(old_str) and old_str[position]!=letter:
                new_str=new_str+letter
                if count>1:
                    new_str=new_str+str(count)
                
                letter=old_str[position]
                count=0
    
    #at the end of the string it adds the count of the last letter outside the loop
    new_str=new_str+letter
    if count>1:
        new_str=new_str+str(count)

    
    return new_str

                


if __name__=="__main__":

    #time complexity
    #details are printed on runnin the program
    print("---------------------------------")
    print("\nTIME COMPLEXITY")
    print("\nSay the string length is n and the number of distinct character in the string is m, then the time complexity would be O(n).")
    print("Both the best and worst case complexities are O(n) and can not be reduced further because in order to compress the string, visiting every character atleast once is mandatory.")
    print("Hence, O(n) is the best time complexity we can acquire and that is what this function has. It visits every character exactly once and thus the time complexity of O(n)")
    print("---------------------------------")
    
    
    string=input('Enter the string to compress: ')
    
    #in case of empty string input
    while not string:
        print('String Empty\n')
        
        #try catch method to avoid errors/exceptions
        try:
            string=input('Enter the string to compress: ')
        except Exception as e:
            print(str(e))
            print('Try Again!\n')

    #compress function is called with the input string as arguemnt
    compressed_string=compression(string)
    print(compressed_string)
