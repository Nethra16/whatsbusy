from django.conf.urls import url
from django.urls import path, include
from django.contrib.auth import views as auth_views

from .views import *
from django_registration.backends.one_step.views import RegistrationView

urlpatterns = [
    path('card-details', cardDetails, name='details'),
    path('accounts/register/', RegistrationView.as_view(success_url='/profile/'),name='register'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name="registration/login.html"), name='user-login'),
    # path('accounts/', include('django_registration.backends.one_step.urls')),
    # path('accounts/', include('django.contrib.auth.urls')),
]