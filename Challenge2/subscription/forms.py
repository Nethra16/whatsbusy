from django.forms import ModelForm
from .models import *

class CardDetailsForm(ModelForm):
    class Meta:
        model = SubscriptionModel
        fields = ['stripe_id']