from django.shortcuts import render, redirect
from django.template.context_processors import csrf
import datetime

from django.conf import settings
from .models import *
from .forms import *
import stripe
# Create your views here.


stripe.api_key = settings.STRIPE_SECRET


def cardDetails(request):

    if request.method == "POST":
        form = CardDetailsForm(request.POST)
        # print('hii123')
        if form.is_valid():    
            print('hiscfdei123')
            customer = stripe.Charge.create(
                amount = 4999,
                currency = "USD",
                description = request.user.username,
                card = form.cleaned.data['stripe_id']
            )

            form.save()
            print('hisrrrrrrrrrrrrrrrrr')
            return redirect('register-success')
    
    else:

        form = CardDetailsForm()
    
    args={}
    args.update(csrf(request))
    args['form'] = form
    args['publishable'] = settings.STRIPE_PUBLISHABLE
    args['months'] = range(1,12)
    args['years'] =  range(2010,2040)
    args['soon'] = datetime.date.today() + datetime.timedelta(days=7)

    return render(request,'subscription/details.html', args)





